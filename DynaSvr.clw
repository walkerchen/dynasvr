; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "dynasvr.h"
LastPage=0

ClassCount=0

ResourceCount=4
Resource1=IDC_DYNASVR
Resource2=IDC_CONTEXTMAIN
Resource3=IDD_ABOUTBOX
Resource4=IDD_DIALOG_SETTING

[DLG:IDD_ABOUTBOX]
Type=1
Class=?
ControlCount=5
Control1=IDOK,button,1342373889
Control2=IDC_STATIC,static,1342177294
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342177294
Control5=IDC_README,button,1342242816

[MNU:IDC_DYNASVR]
Type=1
Class=?
Command1=IDM_EXIT
Command2=IDM_ABOUT
CommandCount=2

[ACL:IDC_DYNASVR]
Type=1
Class=?
Command1=IDM_ABOUT
Command2=IDM_ABOUT
CommandCount=2

[DLG:IDD_DIALOG_SETTING]
Type=1
Class=?
ControlCount=24
Control1=IDCANCEL,button,1342242816
Control2=IDC_INSTALL,button,1342242816
Control3=IDC_REMOVE,button,1342242816
Control4=IDC_STATIC,button,1342177287
Control5=IDC_STATIC,button,1342177287
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_SERVICENAME,edit,1350631552
Control9=IDC_SERVICEEXE,edit,1350631552
Control10=IDC_BROWSE,button,1342242816
Control11=IDC_INTERACTIVE,button,1342242819
Control12=IDC_SERVICESLIST,listbox,1353777411
Control13=IDC_STATIC,static,1342308352
Control14=IDC_MYGROUP_ONLY,button,1342177289
Control15=IDC_LISTWIN32OWN,button,1342177289
Control16=IDC_ALL,button,1342177289
Control17=IDC_BUTTON_ABOUT,button,1342251008
Control18=IDC_STARTUP_BOX,combobox,1344339971
Control19=IDC_STARTUP,static,1342308352
Control20=IDC_SVRCTRL,button,1476460544
Control21=IDC_SVRSTATUS,static,1342308352
Control22=IDC_STATIC,static,1342177294
Control23=IDC_STATIC,static,1342308352
Control24=IDHIDE,button,1342242816

[MNU:IDC_CONTEXTMAIN]
Type=1
Class=?
Command1=ID_MENUITEM32777
Command2=IDI_C_INFO
Command3=IDI_C_START
Command4=IDI_C_STOP
Command5=IDI_C_ABOUT
Command6=IDI_C_RESTORE
Command7=IDCANCEL
CommandCount=7

