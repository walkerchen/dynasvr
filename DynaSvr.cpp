// DynaSvr.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"

// WWWWW
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <Commdlg.h>
#include <shellapi.h>
#include <winuser.h>

#define DllImport   __declspec( dllimport )

// <TRAY>

HICON		mIcon;
BOOL		mIconMinied = FALSE;
BOOL		FirstEntry = TRUE;
NOTIFYICONDATA IconData;

#define UWM_SYSTRAY (WM_USER + 1) // Sent to us by the systray
#define UWM_ACTIVEMSG (WM_USER + 2) // Sent to when window is already active

void CALLBACK TrayIcon(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

#define DEFAULT_RECT_WIDTH 150
#define DEFAULT_RECT_HEIGHT 30

void ShowActiveMsg( HWND hDlg );
static VOID ShowNotifyIcon(HWND hWnd,BOOL bAdd);
VOID MinimizeWndToTray(HWND hWnd);
static BOOL GetDoAnimateMinimize(VOID);
static VOID GetTrayWndRect(LPRECT lpTrayRect);
static VOID ModifyNotifyIcon(HWND hWnd, char *TipInfo, UINT IconID);
void HideWindow( HWND hDlg );
void UnhideWindow( HWND hDlg );
// </TRAY>

typedef DWORD	SC_ENUM_TYPE;
#define SC_ENUM_PROCESS_INFO	0x00000000

typedef struct _SERVICE_STATUS_PROCESS {
  DWORD  dwServiceType;
  DWORD  dwCurrentState;
  DWORD  dwControlsAccepted;
  DWORD  dwWin32ExitCode;
  DWORD  dwServiceSpecificExitCode;
  DWORD  dwCheckPoint;
  DWORD  dwWaitHint;
  DWORD  dwProcessId;
  DWORD  dwServiceFlags;
} SERVICE_STATUS_PROCESS, *LPSERVICE_STATUS_PROCESS;

typedef struct _ENUM_SERVICE_STATUS_PROCESS {
  LPTSTR  lpServiceName;
  LPTSTR  lpDisplayName;
  SERVICE_STATUS_PROCESS  ServiceStatusProcess;
} ENUM_SERVICE_STATUS_PROCESS, *LPENUM_SERVICE_STATUS_PROCESS;

VOID __stdcall MyServiceStart (DWORD argc, LPTSTR *argv); 
VOID __stdcall MyServiceCtrlHandler (DWORD opcode); 
DWORD MyServiceInitialization (DWORD argc, LPTSTR *argv, 
        DWORD *specificError); 

VOID SvcDebugOut(LPSTR String, DWORD Status);

BOOL StopDynawebServer();
BOOL GetSvrSettings();

HWND FindConsoleHandle();

int DoService();

BOOL InstallService( char *inServiceName, char *inServiceExe, BOOL inInteractive );
BOOL RemoveService( char *inServiceName );
BOOL StartMyService();
BOOL StopMyService();
BOOL ListService( int inServiceType );
void FillServiceList( HWND inWnd );
char *ServiceStatusDesc( DWORD inStatus );
char *ServiceTypeDesc( DWORD inType );
void RemoveBlank( char *inStr );
BOOL BrowseFile( HWND hwnd, char *inFileName );
void DoSelChange( HWND hDlg );
BOOL SvrContrl( HWND hDlg );
void SetSvrCtlBtn( HWND SvrCtlBtn );
BOOL SetDefaultSel( HWND inWnd );
BOOL isWinNT();

#define BUFFER_SIZE 512

SERVICE_STATUS          MyServiceStatus; 
SERVICE_STATUS_HANDLE   MyServiceStatusHandle; 
 

char MyServiceName[] = "NT Service controller by bayard@unc.com.cn";
char ApplicationName[] = "NT_SERVICE_CONTROLLER";
char ServiceGroup[] = "DYNAWEB_BAYARD_UNCNET_COM";
char StrDisabled[] = "Popup menu is disabled while main window is active";

char RunningDir[BUFFER_SIZE] = "d:/usr/local/enhydra3.1";

char lpCommandLine[BUFFER_SIZE*2] = "d:/jdk1.2.2/bin/java com.lutris.multiServer.MultiServer d:/usr/local/enhydra3.1/multiserver.conf";
char DebugFileName[BUFFER_SIZE] = "d:/usr/local/enhydra3.1/logs/DynawebIPSSvr.log";

char lpCurrentDirectory[BUFFER_SIZE/2];

FILE *LogFile;
BOOL LogFileFlag = TRUE;

#define DYNAWEB_RUNNING_DIR		0
#define DYNAWEB_CMD				1
#define DYNAWEB_LOG				2
#define DYNAWEB_LOG_FILE		3

char* IniNames[] =
{
	"DYNAWEB_RUNNING_DIR",
	"DYNAWEB_CMD",
	"DYNAWEB_LOG",
	"DYNAWEB_LOG_FILE"
};

PROCESS_INFORMATION pi = 
{
	NULL, NULL, NULL, NULL
};

char *CmdSetting = "/s";

char m_ServiceName[BUFFER_SIZE];
char m_ServiceExe[BUFFER_SIZE];
BOOL m_Interactive = FALSE;

// Service startup type
#define	STARTUP_TYPES	5
enum StartupTypes
{
	ST_BOOT = 0,
	ST_SYSTEM=1,
	ST_AUTOMATIC = 2,
	ST_MANUAL = 3,
	ST_DISABLED = 4
};

char *StartTypeNames[STARTUP_TYPES] = 
{
	"Boot",
	"System",
	"Automatic",
	"Manual",
	"Disabled"
};
DWORD SelStartType = ST_MANUAL;

// Services status buffer

ENUM_SERVICE_STATUS ServiceBuf[ BUFFER_SIZE * 2 ];
ENUM_SERVICE_STATUS_PROCESS ServiceProcBuf[ BUFFER_SIZE * 2 ];

BOOL BufferFilled = FALSE;

DWORD BufferSizeNeeded = 0;
DWORD ReturnEntries = 0;
DWORD ResumePos = 0;

char *SelServiceName = NULL;
char *SelServiceDesc = NULL;
DWORD SelServiceStatus = -1;
DWORD CurrentServiceType = -1;

enum ServiceTypes
{
	ST_DEFAULT = 0,
	ST_MYGROUP = 1,
	ST_WIN32 = 2,
	ST_ALL = 3
};
int InstSelServiceType = 0;

typedef BOOL (CALLBACK *SVRPROC)(
  SC_HANDLE,
  SC_ENUM_TYPE,
  DWORD,
  DWORD,
  LPBYTE,
  DWORD,
  LPDWORD,
  LPDWORD,
  LPDWORD,
  LPCTSTR
);

BOOL UsingDLLCall = FALSE;

HWND DlgWnd = NULL;
// WWWWW

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];								// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];								// The title bar text

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

BOOL ServiceMode = FALSE;

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
	BOOL IsRunningAsApp = FALSE;
	STARTUPINFO si;
	
	if( !isWinNT() )
	{
		MessageBox( NULL, "Sorry, imcompatible OS detected.\nThis program runs under Windows NT/2000 only.", "OS type mismatch", MB_ICONSTOP );

		return 1;
	}

	ZeroMemory( &si, sizeof(STARTUPINFO) );
	si.lpReserved = NULL;
	si.cb = sizeof( STARTUPINFO );
	GetStartupInfo( &si );

	/*
	char TempStr[ BUFFER_SIZE ];
	ZeroMemory( &TempStr, BUFFER_SIZE );
	sprintf( TempStr, "Desktop:%s\nTitle:%s\nFlags:%d\nShowWindow:%d",
		si.lpDesktop,
		si.lpTitle,
		si.dwFlags,
		si.wShowWindow
		);
	MessageBox( NULL, TempStr, "STARTUP INFO", MB_ICONSTOP );
	*/

	IsRunningAsApp = si.wShowWindow;

	// If cmd line set to "/s", then start setting dialog
	if( !IsRunningAsApp && strcmp( _strlwr(CmdSetting), _strlwr( lpCmdLine) ) != 0 )
	{
		ServiceMode = DoService();
	}

	if( !ServiceMode )
	{
		HWND ActiveWnd = FindWindow( NULL, MyServiceName );

		if( ActiveWnd )
		{
			// Set process is already running message and return immediately
			SendNotifyMessage( ActiveWnd, UWM_ACTIVEMSG, 0, 0 );
			return 3;
		}
 		// TODO: Place code here.
		MSG msg;
		HACCEL hAccelTable;

		// Initialize global strings
		LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
		LoadString(hInstance, IDC_DYNASVR, szWindowClass, MAX_LOADSTRING);

		// Setting priority to low
		// SetPriorityClass(GetCurrentProcess(), IDLE_PRIORITY_CLASS);

		MyRegisterClass(hInstance);

		// Perform application initialization:
		if (!InitInstance (hInstance, nCmdShow)) 
		{
			return 2;
		}

		hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_DYNASVR);

		// Main message loop:
		while (GetMessage(&msg, NULL, 0, 0)) 
		{
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}

	return 0;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_DYNASVR);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= (LPCSTR)IDC_DYNASVR;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateDialog( hInstance,
		MAKEINTRESOURCE(IDD_DIALOG_SETTING),
		NULL,
		(DLGPROC)WndProc );

	if (!hWnd)
	{
		return FALSE;
	}

	DlgWnd = hWnd;

	ShowWindow( hWnd, nCmdShow );
	UpdateWindow( hWnd );

	return TRUE;
}

// TrayIcon
void CALLBACK TrayIcon(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	POINT pt;
	HMENU hmenu, hpopup;
	UINT SelItem;
	char TempStr[ BUFFER_SIZE ];

	MENUITEMINFO mii;
	BOOL ErrorCode;

	HWND ServiceListWnd = GetDlgItem( hWnd, IDC_SERVICESLIST );

	ZeroMemory( TempStr, BUFFER_SIZE );
	ZeroMemory( &mii, sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO);

	if( IsWindowVisible( hWnd ) )
		ModifyNotifyIcon( hWnd, StrDisabled, IDI_DISABLED );
	else
		ModifyNotifyIcon( hWnd, MyServiceName, NULL );

	switch (lParam)
	{
		case WM_RBUTTONUP: // Let's track a popup menu
			  if( !IsWindowVisible( hWnd ) )
			  {
				  GetCursorPos(&pt);
				  hmenu = LoadMenu(hInst, MAKEINTRESOURCE(IDC_CONTEXTMAIN));

				  hpopup = GetSubMenu(hmenu, 0);

				  if( SelServiceDesc == NULL || SelServiceName == NULL )
				  {
					  EnableMenuItem( hpopup, IDI_C_START, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED );
					  EnableMenuItem( hpopup, IDI_C_STOP, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED );

					  sprintf( TempStr, "Please select a service from list in main menu to control." );
				  }
				  else
				  {
					  // Pre-handle menu
					  if( SelServiceStatus != SERVICE_RUNNING )
					  {
						  EnableMenuItem( hpopup, IDI_C_START, MF_BYCOMMAND | MF_ENABLED );
						  EnableMenuItem( hpopup, IDI_C_STOP, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED );
					  }
					  else
					  {
						  EnableMenuItem( hpopup, IDI_C_STOP, MF_BYCOMMAND | MF_ENABLED );
						  EnableMenuItem( hpopup, IDI_C_START, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED );
					  }

					  sprintf( TempStr, "Name: %s[%s], type: %s, %s", SelServiceDesc, SelServiceName, ServiceTypeDesc( CurrentServiceType ), ServiceStatusDesc( SelServiceStatus ) );
				  }
					// Get the state of the specified menu item. 

 				  mii.fMask = MIIM_STRING; 
				  mii.dwTypeData = NULL; 
				  ErrorCode = GetMenuItemInfo(hpopup, IDI_C_INFO, FALSE, &mii); 
				  mii.cch = BUFFER_SIZE; 
 
				  mii.fMask = MIIM_STRING; 
				  mii.fType = MFT_STRING; 
				  mii.dwItemData = (UINT)TempStr; 
				  mii.dwTypeData = TempStr; 
				  ErrorCode = SetMenuItemInfo(hpopup, IDI_C_INFO, FALSE, &mii); 

				  DrawMenuBar( hWnd );
    
				  /* SetForegroundWindow and the ensuing null PostMessage is a
					 workaround for a Windows 95 bug (see MSKB article Q135788,
					 http://www.microsoft.com/kb/articles/q135/7/88.htm, I think).
					 In typical Microsoft style this bug is listed as "by design".
					 SetForegroundWindow also causes our MessageBox to pop up in front
					 of any other application's windows. */

				  SetForegroundWindow(hWnd);

				  /* We specifiy TPM_RETURNCMD, so TrackPopupMenu returns the menu
					 selection instead of returning immediately and our getting a
					 WM_COMMAND with the selection. You don't have to do it this way.
				  */
			  
				  SelItem = TrackPopupMenu(hpopup,            // Popup menu to track
									 TPM_RETURNCMD |    // Return menu code
									 TPM_RIGHTBUTTON,   // Track right mouse button?
									 pt.x, pt.y,        // screen coordinates
									 0,                 // reserved
									 hWnd,              // owner
									 NULL);             // LPRECT user can click in
														// without dismissing menu
				  switch( SelItem )
				  {
					case IDCANCEL:
						DestroyWindow(hWnd);
						break;

					case IDI_C_ABOUT:
						DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
						break;

					case IDI_C_START:
					case IDI_C_STOP:
						SvrContrl( hWnd );
						break;

					case IDI_C_RESTORE:
						UnhideWindow( hWnd );
						break;
				  }

				  PostMessage(hWnd, 0, 0, 0); // see above
				  DestroyMenu(hmenu); // Delete loaded menu and reclaim its resources
			  }
			  break;


			case WM_LBUTTONUP:
				if( IsWindowVisible( hWnd ) )
				{
					HideWindow( hWnd );
				}else
				{
					UnhideWindow( hWnd );
				}
				break;

			/*
			case WM_LBUTTONDBLCLK:
				UnhideWindow( hWnd );
				break;
			*/
	}
	return;
}

// Mesage handler for about box.
LRESULT CALLBACK WndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	TCHAR szHello[MAX_LOADSTRING];
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);

	//HWND ServiceListWnd;
	HWND ServiceExeWnd, RadioWnd;
	HWND StartupListWnd;
	char TempStr[ BUFFER_SIZE ];
	DWORD i;

	//ServiceListWnd = GetDlgItem( hDlg, IDC_SERVICESLIST );
	StartupListWnd = GetDlgItem( hDlg, IDC_STARTUP_BOX );

	switch (message)
	{
		case WM_INITDIALOG:
			// Setting up icon
			SetClassLong( hDlg, GCL_HICON, (UINT)LoadIcon(hInst,MAKEINTRESOURCE(IDI_SMALL)) );
			SetWindowText( hDlg, MyServiceName );

			// Get all services belong to this group
			// if( !BufferFilled )
			ListService( ST_MYGROUP );
			FillServiceList( hDlg );

			RadioWnd = GetDlgItem( hDlg, IDC_MYGROUP_ONLY );
			SendMessage( RadioWnd, BM_CLICK, 0, 0 );

			SendMessage( StartupListWnd, CB_RESETCONTENT , 0, 0 );

			for( i=0; i<STARTUP_TYPES; i++ )
			{
				SendMessage( StartupListWnd, CB_ADDSTRING, 0, (WPARAM)StartTypeNames[i] );
			}
			SendMessage( StartupListWnd, CB_SETCURSEL, (WPARAM)ST_MANUAL, 0 );

			if( wParam == SC_RESTORE ) mIconMinied = FALSE;
			else mIconMinied = TRUE;

			// <TRAY>
			ShowNotifyIcon(hDlg, TRUE);
			// </TRAY>

			break;
		case WM_PAINT:
			if( FirstEntry )
			{
				SendMessage( hDlg, WM_SIZE, SIZE_MINIMIZED, 0 );
			}
			break;

		case WM_DESTROY:
			// <TRAY>
			ShowNotifyIcon(hDlg, FALSE);
			// </TRAY>

			PostQuitMessage( 0 );
			break;

		case WM_CLOSE:
			// <TRAY>
			HideWindow( hDlg );
			// </TRAY>
			return TRUE;

			// <TRAY>
		case UWM_SYSTRAY:
			TrayIcon( hDlg, message, wParam, lParam);
			return TRUE;
			// </TRAY>

		case UWM_ACTIVEMSG:
			ShowActiveMsg( hDlg );
			return TRUE;

		/*
		case WM_WINDOWPOSCHANGED:
			MessageBox( hDlg, "WM_WINDOWPOSCHANGED", "SUCCESS", NULL );
			break;

		case WM_MOVE:
			sprintf( TempStr, "MOVE %d:%d:%d", (DWORD)lParam, (int)(short) LOWORD(lParam), (int)(short) HIWORD(lParam) );
			MessageBox( hDlg, TempStr, "SUCCESS", NULL );
			break;
		*/

		case WM_SIZE:
			/*
			sprintf( TempStr, "SIZE MAX=%s:MAXED=%s:MAXSHOW=%s:MIN=%s:MIN=%s",
				(wParam==SIZE_MAXHIDE)?"Y":"N",
				(wParam==SIZE_MAXIMIZED)?"Y":"N",
				(wParam==SIZE_MAXSHOW)?"Y":"N",
				(wParam==SIZE_MINIMIZED)?"Y":"N",
				(wParam==SIZE_RESTORED)?"Y":"N" );
				
			MessageBox( hDlg, TempStr, "SUCCESS", NULL );
			*/
			
			switch( wParam )
			{
				case SIZE_MINIMIZED:
					HideWindow( hDlg );
					return TRUE;

				default:
					break;
			}
			break;

		case WM_SYSCOMMAND:
			switch( wParam )
			{
				case SC_MINIMIZE:
					HideWindow( hDlg );
					return TRUE;

				default:
					break;
			}
			break;

		case WM_COMMAND:
			switch( wParam )
			{
				case IDHIDE:
					HideWindow( hDlg );
					break;

				case IDCANCEL:
					EndDialog(hDlg, LOWORD(wParam));
					// <TRAY>
					IconData.hWnd = hDlg;
					IconData.uFlags = NIF_TIP;
					strcpy( IconData.szTip, "Removal in progress..." );
					IconData.cbSize = sizeof( NOTIFYICONDATA );

					Shell_NotifyIcon( NIM_DELETE, &IconData );
					// </TRAY>
					PostQuitMessage( 0 );
					return TRUE;

				case IDC_BUTTON_ABOUT:
					DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hDlg, (DLGPROC)About);
					return TRUE;

				case IDC_BROWSE:
					if( BrowseFile( hDlg, TempStr ) )
					{
						ServiceExeWnd = GetDlgItem( hDlg, IDC_SERVICEEXE );

						strcpy( m_ServiceExe, TempStr );
						SendMessage( ServiceExeWnd, WM_SETTEXT, 0, (LPARAM)m_ServiceExe );

						UpdateWindow( ServiceExeWnd );
					}
					return TRUE;

				case IDC_INSTALL:
					if( m_ServiceName != NULL && m_ServiceExe != NULL &&
						strlen(m_ServiceName) >0 && strlen(m_ServiceExe) >0 )
					{
						if( InstallService( m_ServiceName, m_ServiceExe, m_Interactive ) )
						{
							MessageBox( hDlg, "Service installed successfully!", "SUCCESS", NULL );
							SvcDebugOut( "Service installed successfully!", 0 );

							// Refresh services list
							ListService( InstSelServiceType );
							FillServiceList( hDlg );

							UpdateWindow( hDlg );

						}
						else
						{
							MessageBox( hDlg, "Service installation failed!", NULL, NULL );
							SvcDebugOut( "Service installation failed!", GetLastError() );
						}
					}
					else
					{
						MessageBox( hDlg, "Both \"Service Name\" and \"Service Exe\" must be specified!", NULL, NULL );
					}
					return TRUE;

				case IDC_REMOVE:
					if( SelServiceDesc == NULL || SelServiceName == NULL )
					{
						MessageBox( hDlg, "Please select an item to remove.", "Comfirm service removal", MB_OK );
					}
					else
					{
						ZeroMemory( TempStr, BUFFER_SIZE );
						sprintf( TempStr, "Are you sure to remove service \"%s\" (%s) ?", SelServiceDesc, SelServiceName );
						if( MessageBox( hDlg, TempStr, "Comfirm service removal", MB_YESNO | MB_ICONEXCLAMATION | MB_DEFBUTTON2 ) == IDYES )
						{

							if( RemoveService( SelServiceName ) )
							{
								MessageBox( hDlg, "Service removed successfully!", "SUCCESS", NULL );
								SvcDebugOut( "Service removed successfully!", 0 );

								// Refresh services list
								ListService( InstSelServiceType );
								FillServiceList( hDlg );

								UpdateWindow( hDlg );

							}
							else
							{
								MessageBox( hDlg, "Service removal failed!", "Failed", NULL );
							}
						}
					}
					return TRUE;

				case IDC_SVRCTRL:
					SvrContrl( hDlg );
					return TRUE;

				default:
					switch( LOWORD(wParam) )
					{
						case IDC_STARTUP_BOX:
							switch( HIWORD( wParam ) )
							{
								case CBN_SELCHANGE:
									SelStartType = SendMessage( StartupListWnd, CB_GETCURSEL, 0, 0 );
									break;

								default:
									break;
							}
							break;

						case IDC_SERVICESLIST:
							switch( HIWORD( wParam ) )
							{
								/*
								case LBN_KILLFOCUS:
									// Service control
									TempWnd = GetDlgItem( hDlg, IDC_SVRCTRL );
									EnableWindow( TempWnd, FALSE );

									TempWnd = GetDlgItem( hDlg, IDC_SERVICESLIST );
									SendMessage( TempWnd, LB_SETSEL, (WPARAM)FALSE, (LPARAM)-1 );
									break;
								*/

								case LBN_SELCHANGE:
									DoSelChange( hDlg );
									break;

								default:
									break;
							}
							break;


						case IDC_INTERACTIVE:
							switch( HIWORD( wParam ) )
							{
								case BN_CLICKED:
									/*
									LRESULT Result;
									char Temp[BUFFER_SIZE];

									UpdateWindow( (HWND)lParam );
									Result = SendDlgItemMessage( hDlg, IDC_INTERACTIVE, BM_GETSTATE, 0, 0);
									sprintf( Temp, "%d", m_Interactive );
									MessageBox( NULL, Temp, NULL, NULL );
									*/
									m_Interactive = IsDlgButtonChecked( hDlg, IDC_INTERACTIVE );
									break;

								default:
									break;
							}

						case IDC_MYGROUP_ONLY:
							switch( HIWORD( wParam ) )
							{
								case BN_CLICKED:
									if( IsDlgButtonChecked( hDlg, IDC_MYGROUP_ONLY ) )
									{
										ListService( ST_MYGROUP );
										FillServiceList( hDlg );
									}
									break;

								default:
									break;
							}

						case IDC_LISTWIN32OWN:
							switch( HIWORD( wParam ) )
							{
								case BN_CLICKED:
									if( IsDlgButtonChecked( hDlg, IDC_LISTWIN32OWN ) )
									{
										ListService( ST_WIN32 );
										FillServiceList( hDlg );
									}
									break;

								default:
									break;
							}
							break;

						case IDC_ALL:
							switch( HIWORD( wParam ) )
							{
								case BN_CLICKED:
									if( IsDlgButtonChecked( hDlg, IDC_ALL ) )
									{
										ListService( ST_ALL );
										FillServiceList( hDlg );
									}
									break;

								default:
									break;
							}
							break;

						case IDC_SERVICENAME:
							switch( HIWORD( wParam ) )
							{
								case EN_CHANGE:
									UpdateWindow( (HWND)lParam );
									GetWindowText( (HWND)lParam, m_ServiceName, BUFFER_SIZE );
									break;

								default:
									break;
							}
							break;

						case IDC_SERVICEEXE:
							switch( HIWORD( wParam ) )
							{
								case EN_CHANGE:
									UpdateWindow( (HWND)lParam );
									GetWindowText( (HWND)lParam, m_ServiceExe, BUFFER_SIZE );
									break;

								default:
									break;
							}
							break;
					}
					break;
			}
			break;
	
		default:
			//return DefWindowProc(hDlg, message, wParam, lParam);
			break;
	}
    return FALSE;
}

// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	char FileName[ BUFFER_SIZE ];
	char PathName[ BUFFER_SIZE ];
	char *CharPos;

	switch (message)
	{
		case WM_INITDIALOG:
				return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			else if( LOWORD(wParam) == IDC_README ) // readme.txt
			{
				ZeroMemory( FileName, BUFFER_SIZE );
				ZeroMemory( PathName, BUFFER_SIZE );
				if( GetModuleFileName( NULL, FileName, BUFFER_SIZE ) != 0 )
				{
					CharPos = strrchr( FileName, '\\' );
					if( CharPos != NULL )
					{
						strncpy( PathName, FileName, CharPos - FileName );
						strcat( PathName, "\\manual.txt" );
						ShellExecute( hDlg, NULL, PathName, NULL, NULL, SW_SHOWDEFAULT );
					}
				}
			}

			break;
	}
    return FALSE;
}

void ShowActiveMsg( HWND hDlg )
{
	BOOL WindowVisibility = IsWindowVisible( hDlg );
	
	ShowWindow( hDlg, SW_SHOW );
	SetActiveWindow( hDlg );
	SetForegroundWindow( hDlg );
	SendMessage( hDlg, WM_SIZE, SIZE_MAXIMIZED, 0 );			

	MessageBox( hDlg, "NT service controller (by bayard@unc.com.cn) programm is already running!", "Program already running", MB_ICONINFORMATION );

	if( !WindowVisibility ) SendMessage( hDlg, WM_SIZE, SIZE_MINIMIZED, 0 );			

	return;
}

void HideWindow( HWND hDlg )
{
	MinimizeWndToTray( hDlg );
	ShowNotifyIcon( hDlg,TRUE );
	ModifyNotifyIcon( hDlg, MyServiceName, NULL );

	// Return TRUE to tell DefDlgProc that we handled the message, and set
	// DWL_MSGRESULT to 0 to say that we handled WM_SYSCOMMAND
	SetWindowLong( hDlg, DWL_MSGRESULT,0 );

	return;
}

void UnhideWindow( HWND hDlg )
{
	if( FirstEntry ) FirstEntry = FALSE;

	ShowWindow( hDlg, SW_SHOW );
	SetActiveWindow(hDlg);
	SetForegroundWindow(hDlg);

	ModifyNotifyIcon( hDlg, StrDisabled, IDI_DISABLED );

	return;
}

static VOID ShowNotifyIcon(HWND hWnd,BOOL bAdd)
{
  NOTIFYICONDATA nid;

  ZeroMemory(&nid,sizeof(nid));
  nid.cbSize = sizeof(NOTIFYICONDATA);
  nid.hWnd = hWnd;
  nid.uID = 0;
  nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
  nid.uCallbackMessage = UWM_SYSTRAY;
  nid.hIcon=LoadIcon(hInst,MAKEINTRESOURCE(IDI_DISABLED));
  lstrcpy(nid.szTip,TEXT(MyServiceName));

  if(bAdd)
    Shell_NotifyIcon(NIM_ADD,&nid);
  else
    Shell_NotifyIcon(NIM_DELETE,&nid);
}

static VOID ModifyNotifyIcon(HWND hWnd, char *TipInfo, UINT IconID)
{
  NOTIFYICONDATA nid;

  ZeroMemory(&nid,sizeof(nid));
  nid.cbSize = sizeof(NOTIFYICONDATA);
  nid.hWnd = hWnd;
  nid.uID = 0;
  nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
  nid.uCallbackMessage = UWM_SYSTRAY;
  if( IconID == NULL ) IconID = IDI_SMALL;
  nid.hIcon=LoadIcon(hInst,MAKEINTRESOURCE(IconID));
  lstrcpy(nid.szTip,TEXT( TipInfo ));

  Shell_NotifyIcon(NIM_MODIFY, &nid);
}

VOID MinimizeWndToTray(HWND hWnd)
{
  if(GetDoAnimateMinimize())
  {
    RECT rcFrom,rcTo;

    // Get the rect of the window. It is safe to use the rect of the whole
    // window - DrawAnimatedRects will only draw the caption
    GetWindowRect(hWnd,&rcFrom);
    GetTrayWndRect(&rcTo);

    // Get the system to draw our animation for us
    DrawAnimatedRects(hWnd,IDANI_CAPTION,&rcFrom,&rcTo);
  }

  // Add the tray icon. If we add it before the call to DrawAnimatedRects,
  // the taskbar gets erased, but doesn't get redrawn until DAR finishes.
  // This looks untidy, so call the functions in this order

  // Hide the window
  ShowWindow(hWnd,SW_HIDE);
}

// Check to see if the animation has been disabled
static BOOL GetDoAnimateMinimize(VOID)
{
  ANIMATIONINFO ai;

  ai.cbSize=sizeof(ai);
  SystemParametersInfo(SPI_GETANIMATION,sizeof(ai),&ai,0);

  return ai.iMinAnimate?TRUE:FALSE;
}

static VOID GetTrayWndRect(LPRECT lpTrayRect)
{
  // First, we'll use a quick hack method. We know that the taskbar is a window
  // of class Shell_TrayWnd, and the status tray is a child of this of class
  // TrayNotifyWnd. This provides us a window rect to minimize to. Note, however,
  // that this is not guaranteed to work on future versions of the shell. If we
  // use this method, make sure we have a backup!
  HWND hShellTrayWnd=FindWindowEx(NULL,NULL,TEXT("Shell_TrayWnd"),NULL);
  if(hShellTrayWnd)
  {
    HWND hTrayNotifyWnd=FindWindowEx(hShellTrayWnd,NULL,TEXT("TrayNotifyWnd"),NULL);
    if(hTrayNotifyWnd)
    {
      GetWindowRect(hTrayNotifyWnd,lpTrayRect);
      return;
    }
  }

  // OK, we failed to get the rect from the quick hack. Either explorer isn't
  // running or it's a new version of the shell with the window class names
  // changed (how dare Microsoft change these undocumented class names!) So, we
  // try to find out what side of the screen the taskbar is connected to. We
  // know that the system tray is either on the right or the bottom of the
  // taskbar, so we can make a good guess at where to minimize to
  APPBARDATA appBarData;
  appBarData.cbSize=sizeof(appBarData);
  if(SHAppBarMessage(ABM_GETTASKBARPOS,&appBarData))
  {
    // We know the edge the taskbar is connected to, so guess the rect of the
    // system tray. Use various fudge factor to make it look good
    switch(appBarData.uEdge)
    {
      case ABE_LEFT:
      case ABE_RIGHT:
	// We want to minimize to the bottom of the taskbar
	lpTrayRect->top=appBarData.rc.bottom-100;
	lpTrayRect->bottom=appBarData.rc.bottom-16;
	lpTrayRect->left=appBarData.rc.left;
	lpTrayRect->right=appBarData.rc.right;
	break;

      case ABE_TOP:
      case ABE_BOTTOM:
	// We want to minimize to the right of the taskbar
	lpTrayRect->top=appBarData.rc.top;
	lpTrayRect->bottom=appBarData.rc.bottom;
	lpTrayRect->left=appBarData.rc.right-100;
	lpTrayRect->right=appBarData.rc.right-16;
	break;
    }

    return;
  }

  // Blimey, we really aren't in luck. It's possible that a third party shell
  // is running instead of explorer. This shell might provide support for the
  // system tray, by providing a Shell_TrayWnd window (which receives the
  // messages for the icons) So, look for a Shell_TrayWnd window and work out
  // the rect from that. Remember that explorer's taskbar is the Shell_TrayWnd,
  // and stretches either the width or the height of the screen. We can't rely
  // on the 3rd party shell's Shell_TrayWnd doing the same, in fact, we can't
  // rely on it being any size. The best we can do is just blindly use the
  // window rect, perhaps limiting the width and height to, say 150 square.
  // Note that if the 3rd party shell supports the same configuraion as
  // explorer (the icons hosted in NotifyTrayWnd, which is a child window of
  // Shell_TrayWnd), we would already have caught it above
  hShellTrayWnd=FindWindowEx(NULL,NULL,TEXT("Shell_TrayWnd"),NULL);
  if(hShellTrayWnd)
  {
    GetWindowRect(hShellTrayWnd,lpTrayRect);
    if(lpTrayRect->right-lpTrayRect->left>DEFAULT_RECT_WIDTH)
      lpTrayRect->left=lpTrayRect->right-DEFAULT_RECT_WIDTH;
    if(lpTrayRect->bottom-lpTrayRect->top>DEFAULT_RECT_HEIGHT)
      lpTrayRect->top=lpTrayRect->bottom-DEFAULT_RECT_HEIGHT;

    return;
  }

  // OK. Haven't found a thing. Provide a default rect based on the current work
  // area
  SystemParametersInfo(SPI_GETWORKAREA,0,lpTrayRect,0);
  lpTrayRect->left=lpTrayRect->right-DEFAULT_RECT_WIDTH;
  lpTrayRect->top=lpTrayRect->bottom-DEFAULT_RECT_HEIGHT;
}

// Selection change
void DoSelChange( HWND hDlg )
{
	HWND ServiceListWnd;

	DWORD SelItem, SvrPos;
	HWND TempWnd;
	char TempStr[ BUFFER_SIZE ];

    ServiceListWnd =  GetDlgItem( hDlg, IDC_SERVICESLIST ); 
	TempWnd = GetDlgItem( hDlg, IDC_SVRSTATUS );

	SelItem = SendMessage(ServiceListWnd, LB_GETCURSEL, (WPARAM)0, (LPARAM)0 ); 
	SvrPos = SendMessage( ServiceListWnd, LB_GETITEMDATA, (WPARAM)SelItem, (LPARAM)0 );

	if( SvrPos < 0 || SvrPos >= ReturnEntries )
	{
		ZeroMemory( TempStr, BUFFER_SIZE );
		sprintf( TempStr, "No service is currently selected.\nPlease select a service to process." );
		SetWindowText( TempWnd, TempStr );
		return;
	}

	if( UsingDLLCall ) SelServiceDesc = ServiceProcBuf[SvrPos].lpDisplayName;
	else SelServiceDesc = ServiceBuf[SvrPos].lpDisplayName;

	if( UsingDLLCall ) SelServiceName = ServiceProcBuf[SvrPos].lpServiceName;
	else SelServiceName = ServiceBuf[SvrPos].lpServiceName;

	if( TempWnd )
	{
		DWORD _status, _type;

		if( UsingDLLCall )
		{
			_status = ServiceProcBuf[SvrPos].ServiceStatusProcess.dwCurrentState;
			_type = ServiceProcBuf[SvrPos].ServiceStatusProcess.dwServiceType;
		}
		else
		{
			_status = ServiceBuf[SvrPos].ServiceStatus.dwCurrentState;
			_type =  ServiceBuf[SvrPos].ServiceStatus.dwServiceType;
		}

		SelServiceStatus = _status;
		CurrentServiceType = _type; 

		ZeroMemory( TempStr, BUFFER_SIZE );
		sprintf( TempStr, "Service type: %s\n[%s] Status: %s", ServiceTypeDesc( _type ), SelServiceName ,ServiceStatusDesc( _status ) );

		SetWindowText( TempWnd, TempStr );

		SetSvrCtlBtn( GetDlgItem( hDlg, IDC_SVRCTRL ) );
	}
	return;
}

BOOL SvrContrl( HWND hDlg )
{
	HWND ServiceListWnd, SvrCtlBtn;

	BOOL ErrorCode;
	char TempStr[ BUFFER_SIZE ];

	ServiceListWnd = GetDlgItem( hDlg, IDC_SERVICESLIST );
	SvrCtlBtn = GetDlgItem( hDlg, IDC_SVRCTRL );

	if( SelServiceDesc == NULL || SelServiceName == NULL )
	{
		MessageBox( hDlg, "Please select an service to control.", "Service not selected", MB_OK );
	}
	else
	{
		ZeroMemory( TempStr, BUFFER_SIZE );

		if( SelServiceStatus != SERVICE_RUNNING )
		{
			if( (ErrorCode = StartMyService()) )
			{
				sprintf( TempStr, "Service \"%s\" started successfully!", SelServiceName );

				SelServiceStatus = SERVICE_RUNNING;
			}
			else
			{
				sprintf( TempStr, "Service \"%s\" failed to start!", SelServiceName );
			}
		}
		else
		{
			if( (ErrorCode = StopMyService()) )
			{
				sprintf( TempStr, "Service \"%s\" stopped successfully!", SelServiceDesc );

				SelServiceStatus = SERVICE_STOPPED;
			}
			else
			{
				sprintf( TempStr, "Service \"%s\" failed to stop!", SelServiceDesc );
			}
		}
		MessageBox( hDlg, TempStr, "Service control information", MB_ICONINFORMATION );

		// Refresh services list
		ListService( InstSelServiceType );
		FillServiceList( ServiceListWnd );

		// Setting service control button status
		SetSvrCtlBtn( SvrCtlBtn );

		// Setting default select
		// SetDefaultSel( hDlg );
	
		// Selected item change
		DoSelChange( hDlg );

		UpdateWindow( hDlg );
	}

	return ErrorCode;
}

void SetSvrCtlBtn( HWND SvrCtlBtn )
{
	EnableWindow( SvrCtlBtn, TRUE );

	if( SelServiceStatus != SERVICE_RUNNING )
	{
		SetWindowText( SvrCtlBtn, "Start service" );
	}
	else
	{
		SetWindowText( SvrCtlBtn, "Stop service" );
	}

	return;
}

void FillServiceList( HWND inWnd )
{
	DWORD i, NewPos;
	char *p;

	HWND ServiceListWnd;

	ServiceListWnd = GetDlgItem( inWnd, IDC_SERVICESLIST );

	// Initialize list
	SendMessage( ServiceListWnd, LB_RESETCONTENT, 0, 0 );
	SelServiceName = NULL;
	SelServiceDesc = NULL;

	for( i=0; i< ReturnEntries; i++ )
	{
		if( UsingDLLCall ) p = ServiceProcBuf[i].lpDisplayName;
		else p = ServiceBuf[i].lpDisplayName;
		NewPos = SendMessage( ServiceListWnd,	LB_ADDSTRING, (WPARAM) 0, (LPARAM) p); 
		SendMessage( ServiceListWnd,	LB_SETITEMDATA, (WPARAM) NewPos, (LPARAM) i); 
	}

	SetDefaultSel( ServiceListWnd );
	DoSelChange( inWnd );

	UpdateWindow( ServiceListWnd );

}

BOOL SetDefaultSel( HWND inWnd )
{
	DWORD _status, _type;

	// Set default select to first entry
	if( ReturnEntries > 0 )
	{
		SendMessage( inWnd, LB_SETCURSEL, 0, 0 );
		if( UsingDLLCall ) SelServiceName = ServiceProcBuf[0].lpServiceName;
		else SelServiceName = ServiceBuf[0].lpServiceName;

		if( UsingDLLCall ) SelServiceDesc = ServiceProcBuf[0].lpDisplayName;
		else SelServiceDesc = ServiceBuf[0].lpDisplayName;
		
		if( UsingDLLCall )
		{
			_status = ServiceProcBuf[0].ServiceStatusProcess.dwCurrentState;
			_type = ServiceProcBuf[0].ServiceStatusProcess.dwServiceType;
		}
		else
		{
			_status = ServiceBuf[0].ServiceStatus.dwCurrentState;
			_type =  ServiceBuf[0].ServiceStatus.dwServiceType;
		}

		SelServiceStatus = _status;
		CurrentServiceType = _type; 
	}

	return TRUE;
}

// WWWWW
// Begin service

BOOL DoService() 
{ 
	/*
	int response = MessageBox( FindConsoleHandle(), 
                           "File read error, continue?", 
                           "File Error", 
                           MB_ICONERROR | MB_YESNO);
	*/

	BOOL ReturnCode = TRUE;

	// Getting service program settings
	GetSvrSettings();

    SERVICE_TABLE_ENTRY   DispatchTable[] = 
    { 
        { MyServiceName, MyServiceStart      }, 
        { NULL,              NULL          } 
    }; 
 
	SvcDebugOut("Starting services ... ", GetLastError()); 

	if (!StartServiceCtrlDispatcher( DispatchTable)) 
    { 
        SvcDebugOut("StartServiceCtrlDispatcher error = ", GetLastError()); 
		ReturnCode = FALSE;
    } 
	else
	{
		SvcDebugOut("StartServiceCtrlDispatcher success. ", GetLastError()); 
		ReturnCode = TRUE;
	}

	SvcDebugOut("------- Service terminated -------", 0 ); 

	if( LogFileFlag )
		if( LogFile != NULL ) fclose( LogFile );

	return( ReturnCode );
} 
 
VOID SvcDebugOut(LPSTR String, DWORD Status) 
{ 
    CHAR  Buffer[1024]; 
    if (strlen(String) < 1000) 
    { 
        sprintf(Buffer, "%s %d\n", String, Status); 
        OutputDebugStringA(Buffer); 
    }

	if( LogFileFlag )
		if( LogFile != NULL )
			fprintf( LogFile, "%s %d\n", String, Status );
} 

void __stdcall MyServiceStart (DWORD argc, LPTSTR *argv) 
{ 
    DWORD status; 
    DWORD specificError; 
 
    MyServiceStatus.dwServiceType        = SERVICE_WIN32; 
    MyServiceStatus.dwCurrentState       = SERVICE_START_PENDING; 
    MyServiceStatus.dwControlsAccepted   = SERVICE_ACCEPT_STOP | 
        SERVICE_ACCEPT_PAUSE_CONTINUE; 
    MyServiceStatus.dwWin32ExitCode      = 0; 
    MyServiceStatus.dwServiceSpecificExitCode = 0; 
    MyServiceStatus.dwCheckPoint         = 0; 
    MyServiceStatus.dwWaitHint           = 0; 
 
    MyServiceStatusHandle = RegisterServiceCtrlHandler( 
        MyServiceName, MyServiceCtrlHandler); 
 
    if (MyServiceStatusHandle == (SERVICE_STATUS_HANDLE)0) 
    { 
        SvcDebugOut("RegisterServiceCtrlHandler failed ", GetLastError()); 
        return; 
    } 
 
    // Initialization code goes here. 
    status = MyServiceInitialization(argc,argv, &specificError); 
 
    // Handle error condition 
    if (status != NO_ERROR) 
    { 
        MyServiceStatus.dwCurrentState       = SERVICE_STOPPED; 
        MyServiceStatus.dwCheckPoint         = 0; 
        MyServiceStatus.dwWaitHint           = 0; 
        MyServiceStatus.dwWin32ExitCode      = status; 
        MyServiceStatus.dwServiceSpecificExitCode = specificError; 
 
        SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus); 
        return; 
    } 
 
    // Initialization complete - report running status. 
    MyServiceStatus.dwCurrentState       = SERVICE_RUNNING; 
    MyServiceStatus.dwCheckPoint         = 0; 
    MyServiceStatus.dwWaitHint           = 0; 
 
    if (!SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus)) 
    { 
        status = GetLastError(); 
        SvcDebugOut("SetServiceStatus error %ld",status); 
    } 
 
    // This is where the service does its work. 

    SvcDebugOut("Returning the Main Thread",0); 
 
    return; 
} 
 
// Stub initialization function. 
DWORD MyServiceInitialization(DWORD   argc, LPTSTR  *argv, 
    DWORD *specificError) 
{ 
    argv; 
    argc; 
    specificError; 

	// WWWWW
	STARTUPINFO si;

	// Set up the start up info struct.
	ZeroMemory(&si,sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = NULL;
	si.hStdOutput = NULL;
	si.hStdInput  = NULL;
	si.hStdError  = NULL;

	LPCTSTR lpApplicationName = NULL;

    BOOL IsSuccess = CreateProcess(
		lpApplicationName,
		lpCommandLine,
		NULL,
		NULL,
		TRUE,
		//CREATE_NEW_CONSOLE,
		NULL,
		NULL,
		lpCurrentDirectory,
		&si,
		&pi
		);

	if( IsSuccess )
	{
		SvcDebugOut( "Owner PID = ", GetCurrentProcessId() );
		SvcDebugOut( "Process ID = ", pi.dwProcessId );
		SvcDebugOut( "Thread ID = ", pi.dwThreadId );
		SvcDebugOut( "Process HANDLE ID = ", (DWORD)pi.hProcess );
		SvcDebugOut( "Thread HANDLE ID = ", (DWORD)pi.hThread );
	}

	// WWWWW

    return(0); 
} 

VOID __stdcall MyServiceCtrlHandler (DWORD Opcode) 
{ 
    DWORD status; 
 
    switch(Opcode) 
    { 
        case SERVICE_CONTROL_PAUSE: 
        // Do whatever it takes to pause here. 
            MyServiceStatus.dwCurrentState = SERVICE_PAUSED; 
            break; 
 
        case SERVICE_CONTROL_CONTINUE: 
        // Do whatever it takes to continue here. 
            MyServiceStatus.dwCurrentState = SERVICE_RUNNING; 
            break; 
 
        case SERVICE_CONTROL_STOP: 
        // Do whatever it takes to stop here. 
			
			// Stop Dynaweb Java Server
			if( StopDynawebServer() )
			{
				SvcDebugOut( "Dynaweb Server terminated succeed with code: ", 0 );
			}
			else
			{
				SvcDebugOut( "Failed to shutdown Dynaweb Server with code: ", GetLastError() );
			}

            MyServiceStatus.dwWin32ExitCode = 0; 
            MyServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
            MyServiceStatus.dwCheckPoint    = 0; 
            MyServiceStatus.dwWaitHint      = 0; 
 
            if (!SetServiceStatus (MyServiceStatusHandle, 
                &MyServiceStatus))
            { 
                status = GetLastError(); 
                SvcDebugOut("SetServiceStatus error %ld",status); 
            } 
 
            SvcDebugOut("Leaving Dynaweb IPS",0); 
            return; 
 
        case SERVICE_CONTROL_INTERROGATE: 
        // Fall through to send current status. 
            break; 
 
        default: 
            SvcDebugOut("Unrecognized opcode %ld", Opcode); 
    } 
 
    // Send current status. 
    if (!SetServiceStatus (MyServiceStatusHandle,  &MyServiceStatus)) 
    { 
        status = GetLastError(); 
        SvcDebugOut("SetServiceStatus error %ld",status); 
    } 
    return; 
} 

BOOL StopDynawebServer()
{
	if( pi.hProcess != NULL )
	{
		return TerminateProcess( pi.hProcess, 0 );
	}
	else return FALSE;
}

BOOL GetSvrSettings()
{
	char FileName[BUFFER_SIZE];
	char PathName[BUFFER_SIZE];
	char *CharPos;

	ZeroMemory( FileName, BUFFER_SIZE );
	ZeroMemory( PathName, BUFFER_SIZE );
	if( GetModuleFileName( NULL, FileName, BUFFER_SIZE ) != 0 )
	{
		CharPos = strrchr( FileName, '.' );
		if( CharPos != NULL )
		{
			strncpy( PathName, FileName, CharPos - FileName );
			strcat( PathName, ".ini" );
			SvcDebugOut( PathName, 0 );

			// Open .ini file and read variables

			char TempStr[BUFFER_SIZE];

			ZeroMemory( TempStr, BUFFER_SIZE );
			GetPrivateProfileString(
				ApplicationName,
				IniNames[ DYNAWEB_LOG ],
				"FALSE",
				TempStr,
				BUFFER_SIZE,
				PathName);
			if( _strnicmp( TempStr, "TRUE", strlen("TRUE") ) == 0 )
				LogFileFlag = TRUE;
			else
				LogFileFlag = FALSE;

			ZeroMemory( TempStr, BUFFER_SIZE );
			GetPrivateProfileString(
				ApplicationName,
				IniNames[ DYNAWEB_LOG_FILE ],
				DebugFileName,
				TempStr,
				BUFFER_SIZE,
				PathName);
			strcpy( DebugFileName, TempStr );

			if( LogFileFlag )
				LogFile = fopen( DebugFileName, "a" );

			SvcDebugOut( "Setting debugging to ", LogFileFlag );
			SvcDebugOut( DebugFileName, 0 );


			ZeroMemory( TempStr, BUFFER_SIZE );
			GetPrivateProfileString(
				ApplicationName,
				IniNames[ DYNAWEB_RUNNING_DIR ],
				RunningDir,
				TempStr,
				BUFFER_SIZE,
				PathName);
			strcpy( RunningDir, TempStr );
			strcpy( lpCurrentDirectory, RunningDir );
			SvcDebugOut( RunningDir, 1 );

			ZeroMemory( TempStr, BUFFER_SIZE );
			GetPrivateProfileString(
				ApplicationName,
				IniNames[ DYNAWEB_CMD ],
				lpCommandLine,
				TempStr,
				BUFFER_SIZE,
				PathName);
			strcpy( lpCommandLine, TempStr );
			SvcDebugOut( lpCommandLine, 2 );
		}
	}

	/*
	// Get environment variables
	char *TempStr;

	// Enhydra Log File
	TempStr = getenv( IniNames[ DYNAWEB_LOG_FILE ] );
	if( TempStr != NULL )
	{
		strcpy( DebugFileName, TempStr );
		LogFileFlag = TRUE;
		SvcDebugOut( TempStr, 0 );
	}

	// Enhydra Home
	TempStr = getenv( IniNames[ DYNAWEB_RUNNING_DIR ] );
	if( TempStr != NULL )
	{
		strcpy( RunningDir, TempStr );
		SvcDebugOut( RunningDir, 0 );
	}

	// Enhydra Cmd
	TempStr = getenv( IniNames[ DYNAWEB_CMD ] );
	if( TempStr != NULL )
	{
		strcpy( lpCommandLine, TempStr );
		SvcDebugOut( lpCommandLine, 0 );
	}
	*/

	return TRUE;
}

HWND FindConsoleHandle()
 {
  static LPCTSTR temptitle = "{98C1C303-2A9E-11d4-9FF5-006067718D04}";
  TCHAR title[512];
  if(GetConsoleTitle(title, sizeof(title)/sizeof(TCHAR)) == 0)
    return NULL;
  SetConsoleTitle(temptitle);
  HWND wnd = FindWindow(NULL, temptitle);
  SetConsoleTitle(title);
  return wnd;
 }

BOOL InstallService( char *inServiceName, char *inServiceExe, BOOL inInteractive )
{
	SC_HANDLE newService, scm;
	char pServiceName[ BUFFER_SIZE ];
	BOOL ErrorCode = TRUE;
	DWORD ServiceType;
	DWORD StartType;

	/*
	sprintf( pServiceName, "%s, %s, %d, %d", inServiceName, inServiceExe, inInteractive, SelStartType );
	MessageBox( DlgWnd, pServiceName, "INFO", 0 );
	return FALSE;
	*/

	ZeroMemory( pServiceName, BUFFER_SIZE );
	strcpy( pServiceName, inServiceName );
	RemoveBlank( pServiceName );

	sprintf( pServiceName, "%s%s", pServiceName, "_SVR" );
 
	// open a connection to the SCM
	scm = OpenSCManager(0, 0, SC_MANAGER_CREATE_SERVICE);
	if (!scm)
	{
		SvcDebugOut("In OpenScManager", GetLastError());
		ErrorCode = TRUE;
		return ErrorCode;
	}

	if( inInteractive )
	{
		ServiceType = SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS;
	}
	else
	{
		ServiceType = SERVICE_WIN32_OWN_PROCESS;
	}

	switch( SelStartType )
	{
		case ST_BOOT:
			StartType = SERVICE_BOOT_START;
			break;

		case ST_SYSTEM:
			StartType = SERVICE_SYSTEM_START;
			break;

		case ST_AUTOMATIC:
			StartType = SERVICE_AUTO_START;
			break;

		default:
		case ST_MANUAL:
			StartType = SERVICE_DEMAND_START;
			break;

		case ST_DISABLED:
			StartType = SERVICE_DISABLED;
			break;

	}

	// Install the new service
	newService = CreateService(
		scm, 
		pServiceName,									// eg "beep_srv"
		inServiceName,									// eg "Beep Service"
		SERVICE_ALL_ACCESS,
		ServiceType,
		StartType,
		SERVICE_ERROR_NORMAL,
		inServiceExe,									// eg "c:\winnt\xxx.exe"
		ServiceGroup,
		0, 0, 0, 0);
	if (!newService)
	{
		SvcDebugOut("In CreateService", GetLastError());
		ErrorCode = FALSE;
	}
	else 
	{
		SvcDebugOut("Service installed successfully", 0);
		ErrorCode = TRUE;
	}
 
	// clean up
	CloseServiceHandle(newService);
	CloseServiceHandle(scm);

	return ErrorCode;
}

// Remove sevice
BOOL RemoveService( char *inServiceName )
{
	SC_HANDLE service, scm;
	BOOL success;
	SERVICE_STATUS status;
 
	// Open a connection to the SCM
	scm = OpenSCManager(0, 0, SC_MANAGER_CREATE_SERVICE);
	if( !scm )
		SvcDebugOut("In OpenScManager", GetLastError());
 
	// Get the service's handle
	service = OpenService(scm, inServiceName, SERVICE_ALL_ACCESS | DELETE);
	if( !service )
		SvcDebugOut("In OpenScManager", GetLastError());
	
	// Stop the service if necessary
	success = QueryServiceStatus(service, &status);
	if (!success)
		SvcDebugOut("In QueryServiceStatus", GetLastError());
	if (status.dwCurrentState != SERVICE_STOPPED)
	{
		success = ControlService(service, SERVICE_CONTROL_STOP, &status);
		if (!success)
			SvcDebugOut("In ControlService", GetLastError());
	}
	
	// Remove the service
	success = DeleteService(service);
	if (success) SvcDebugOut("Service successfully removed!", GetLastError());
	else  SvcDebugOut("Service remove failed!", GetLastError());
 
	// Clean up
	CloseServiceHandle(service);
	CloseServiceHandle(scm);

	return success;
}

BOOL StopMyService()
{
	SC_HANDLE schService, scm;
  	DWORD fdwControl = SERVICE_CONTROL_STOP;
    SERVICE_STATUS ssStatus; 
    DWORD fdwAccess; 
 
    // The required service object access depends on the control. 
 
    fdwAccess = SERVICE_STOP; 

	// Open a connection to the SCM
	scm = OpenSCManager(0, 0, SC_MANAGER_ALL_ACCESS);
	if( !scm )
	{
		SvcDebugOut("In OpenScManager", GetLastError());
		return FALSE;
	}

    // Open a handle to the service. 
 
    schService = OpenService( 
        scm,        // SCManager database 
        SelServiceName,        // name of service 
        fdwAccess);          // specify access 
    if (schService == NULL) 
	{
        SvcDebugOut("In OpenService", GetLastError());
		return FALSE;
	}
 
    // Send a control value to the service. 
 
    if (! ControlService( 
            schService,   // handle to service 
            fdwControl,   // control value to send 
            &ssStatus) )  // address of status info 
    {
        SvcDebugOut("Error in control service.", GetLastError());
		return FALSE;
    }
 
    CloseServiceHandle(schService); 
	CloseServiceHandle(scm);

	return TRUE;
}

BOOL StartMyService() 
{ 
	SC_HANDLE schService, scm;
    SERVICE_STATUS ssStatus; 
    DWORD dwOldCheckPoint; 
    DWORD dwStartTickCount;
    DWORD dwWaitTime;

	// MessageBox( NULL, "Starting...", SelServiceName, NULL );
 
	// Open a connection to the SCM
	scm = OpenSCManager(0, 0, SC_MANAGER_ALL_ACCESS);
	if( !scm )
	{
		SvcDebugOut("In OpenScManager", GetLastError());
		return FALSE;
	}

    schService = OpenService( 
        scm,          // SCM database 
        SelServiceName,          // service name
        SERVICE_ALL_ACCESS); 
 
    if (schService == NULL) 
    { 
        SvcDebugOut("In OpenScManager", GetLastError());
		return FALSE;
    }
 
    if (!StartService(
            schService,  // handle to service 
            0,           // number of arguments 
            NULL) )      // no arguments 
    {
        SvcDebugOut("In StartService", GetLastError());
		return FALSE;
    }
    else 
    {
        SvcDebugOut("Start service in progress...", GetLastError());
    }
 
    // Check the status until the service is no longer start pending. 
 
    if (!QueryServiceStatus( 
            schService,   // handle to service 
            &ssStatus) )  // address of status information structure
    {
        SvcDebugOut("Can't query service name.", GetLastError());
		return FALSE;
    }
 
    // Save the tick count and initial checkpoint.

    dwStartTickCount = GetTickCount();
    dwOldCheckPoint = ssStatus.dwCheckPoint;

    while (ssStatus.dwCurrentState == SERVICE_START_PENDING) 
    { 
        // Do not wait longer than the wait hint. A good interval is 
        // one tenth the wait hint, but no less than 1 second and no 
        // more than 10 seconds. 
 
        dwWaitTime = ssStatus.dwWaitHint / 10;

        if( dwWaitTime < 1000 )
            dwWaitTime = 1000;
        else if ( dwWaitTime > 10000 )
            dwWaitTime = 10000;

        Sleep( dwWaitTime );

        // Check the status again. 
 
        if (!QueryServiceStatus( 
                schService,   // handle to service 
                &ssStatus) )  // address of structure
            break; 
 
        if ( ssStatus.dwCheckPoint > dwOldCheckPoint )
        {
            // The service is making progress.

            dwStartTickCount = GetTickCount();
            dwOldCheckPoint = ssStatus.dwCheckPoint;
        }
        else
        {
            if(GetTickCount()-dwStartTickCount > ssStatus.dwWaitHint)
            {
                // No progress made within the wait hint
                break;
            }
        }
    } 

    CloseServiceHandle(schService); 
	CloseServiceHandle(scm);

    if (ssStatus.dwCurrentState == SERVICE_RUNNING) 
    {
		SvcDebugOut("Service started successfully.", GetLastError());    
		return TRUE;
	}
    else 
    { 
		SvcDebugOut("Service failed to start.", GetLastError());    
		return FALSE;
    } 

	return FALSE;
} 

// List services
BOOL ListService( int inServiceType )
{

	SC_HANDLE scm;
	BOOL success = FALSE;

	DWORD ServiceType = SERVICE_WIN32;
	char *GroupName = NULL;

	InstSelServiceType = inServiceType;

	switch( inServiceType )
	{
		case ST_DEFAULT:	// default
		case ST_MYGROUP:	// List SERVICE_WIN32
			GroupName = ServiceGroup;
		case ST_WIN32:		// List SERVICE_WIN32
		default:
			ServiceType = SERVICE_WIN32;
			break;

		case ST_ALL:
			ServiceType = SERVICE_WIN32 | SERVICE_DRIVER;
			break;

	}
 
	// Open a connection to the SCM
	scm = OpenSCManager(0, 0, SC_MANAGER_ENUMERATE_SERVICE);
	if( !scm )
		SvcDebugOut("In OpenScManager", GetLastError());

	// Load advapi32.dll
	HINSTANCE hinstLib; 
    SVRPROC EnumProc;	

    hinstLib = LoadLibrary("advapi32");
	if (hinstLib != NULL)
	{
		// Get services status
		EnumProc = (SVRPROC) GetProcAddress(hinstLib, "EnumServicesStatusExA");  
		if( EnumProc != NULL )
		{
			success = (EnumProc)
				(
				scm,
				SC_ENUM_PROCESS_INFO,
				ServiceType,
				SERVICE_STATE_ALL,
				(LPBYTE)ServiceProcBuf,
				BUFFER_SIZE * 2 * sizeof( ENUM_SERVICE_STATUS_PROCESS ),
				&BufferSizeNeeded,
				&ReturnEntries,
				&ResumePos,
				GroupName
				);

			/*
			if( !success )
			{
				MessageBox( DlgWnd, "ADAPI32.DLL loaded and EnumServicesStatusExA() founded but invoke failed. ", "Warring", 0 );
			}
			*/
		}
		else
			MessageBox( DlgWnd, "ADAPI32.DLL loaded but EnumServicesStatusExA() not founded.", "Warring", 0 );

		FreeLibrary(hinstLib);

		UsingDLLCall = TRUE;
	}
	else
	{
		MessageBox( DlgWnd, "ADAPI32.DLL could not be loaded. Using EnumServicesStatus() instead.", "Warring", 0 );

		// Get services status
		success = EnumServicesStatus
			(
			scm,
			ServiceType,
			SERVICE_STATE_ALL,
			ServiceBuf,
			BUFFER_SIZE * 2 * sizeof( ENUM_SERVICE_STATUS ),
			&BufferSizeNeeded,
			&ReturnEntries,
			&ResumePos
			);

		UsingDLLCall = FALSE;
	}

	/*
	char Temp[ BUFFER_SIZE ];
	sprintf( Temp, "%d needed, %d items retured, %d ResumePos, ", BufferSizeNeeded, ReturnEntries, ResumePos );
	MessageBox( DlgWnd, Temp, "INFO", 0 );
	*/

	if (!success)
	{
		SvcDebugOut("In EnumServicesStatus", GetLastError());
	}
	else
	{
		SvcDebugOut("List services succeed.", GetLastError());
		BufferFilled = TRUE;
	}
	
	// Clean up
	CloseServiceHandle(scm);

	return success;
}

char *ServiceTypeDesc( DWORD inType )
{
	char TempStr[BUFFER_SIZE];
	static char Desc[BUFFER_SIZE];

	ZeroMemory( TempStr, BUFFER_SIZE );
	ZeroMemory( Desc, BUFFER_SIZE );
	sprintf( TempStr,  "%d, ", inType );
	if( inType & SERVICE_WIN32_OWN_PROCESS ) sprintf( TempStr,  "%s%s ", TempStr, "Win32" );
	if( inType & SERVICE_WIN32_SHARE_PROCESS ) sprintf( TempStr,  "%s%s ", TempStr, "Win32Share" );
	if( inType & SERVICE_KERNEL_DRIVER ) sprintf( TempStr,  "%s%s ", TempStr, "KernelDrv" );
	if( inType & SERVICE_FILE_SYSTEM_DRIVER ) sprintf( TempStr,  "%s%s ", TempStr, "SystemDrv" );
	if( inType & SERVICE_INTERACTIVE_PROCESS  ) sprintf( TempStr,  "%s%s ", TempStr, "Interactive" );

	if( strlen( TempStr ) > 0 ) strncpy( Desc, TempStr, strlen(TempStr)-1 );

	return Desc;
}

char *ServiceStatusDesc( DWORD inStatus )
{
	char TempStr[BUFFER_SIZE];
	static char Desc[BUFFER_SIZE];

	ZeroMemory( TempStr, BUFFER_SIZE );
	ZeroMemory( Desc, BUFFER_SIZE );
	sprintf( TempStr,  "%d, ", inStatus );
	if( inStatus == SERVICE_STOPPED ) sprintf( TempStr, "%s%s ", TempStr, "Stopped" );
	if( inStatus == SERVICE_RUNNING ) sprintf( TempStr,  "%s%s ", TempStr, "Running" );
	if( inStatus == SERVICE_PAUSED  ) sprintf( TempStr,  "%s%s ", TempStr, "Paused" );
	if( inStatus == SERVICE_START_PENDING ) sprintf( TempStr,  "%s%s ", TempStr, "StartPending" );
	if( inStatus == SERVICE_STOP_PENDING ) sprintf( TempStr,  "%s%s ", TempStr, "StopPending" );
	if( inStatus == SERVICE_CONTINUE_PENDING  ) sprintf( TempStr, "%s%s ", TempStr, "ContinuePending" );

	if( strlen( TempStr ) > 0 ) strncpy( Desc, TempStr, strlen(TempStr)-1 );

	return Desc;
}

void RemoveBlank( char *inStr )
{
	while( *inStr )
	{
		if( *inStr == ' ' )	*inStr = '_';
		if( *inStr == '!' )	*inStr = '_';
		if( *inStr == '@' )	*inStr = '_';
		if( *inStr == '#' )	*inStr = '_';
		if( *inStr == '$' )	*inStr = '_';
		if( *inStr == '%' )	*inStr = '_';
		if( *inStr == '^' )	*inStr = '_';
		if( *inStr == '&' )	*inStr = '_';
		if( *inStr == '*' )	*inStr = '_';
		if( *inStr == '(' )	*inStr = '_';
		if( *inStr == ')' )	*inStr = '_';
		if( *inStr == '\\' ) *inStr = '_';
		if( *inStr == '/' )	*inStr = '_';
		if( *inStr == ',' )	*inStr = '_';
		if( *inStr == '<' )	*inStr = '_';
		if( *inStr == '>' )	*inStr = '_';
		if( *inStr == '|' )	*inStr = '_';
		inStr++;
	}

	return;
}

BOOL BrowseFile( HWND hwnd, char *inFileName )
{
	OPENFILENAME ofn;       // common dialog box structure
	char szFile[BUFFER_SIZE];       // buffer for file name

	// Initialize OPENFILENAME
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ZeroMemory(szFile, BUFFER_SIZE );
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = "Executable/Binary files (*.exe,*.com)\0*.exe;*.com\0All\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_EXPLORER | OFN_NONETWORKBUTTON | OFN_READONLY;

	// Display the Open dialog box. 

	if (GetOpenFileName(&ofn)==TRUE) 
	{
		strcpy( inFileName, szFile );
	}
	else inFileName = NULL;

	GetLastError();

	if( inFileName == NULL ) return FALSE;
	return TRUE;
}

BOOL isWinNT()
{
    OSVERSIONINFO osvi;
    memset(&osvi, 0, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
    GetVersionEx (&osvi);

    if (osvi.dwPlatformId == VER_PLATFORM_WIN32s) return FALSE;
    else if (osvi.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS) return FALSE;
    else if (osvi.dwPlatformId == VER_PLATFORM_WIN32_NT) return TRUE; // NT or 2000
    return FALSE;
} 
