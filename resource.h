//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DynaSvr.rc
//
#define IDC_MYICON                      2
#define IDHIDE                          3
#define IDD_DYNASVR_DIALOG              102
#define IDD_ABOUTBOX                    103
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDS_HELLO                       106
#define IDI_DYNASVR                     107
#define IDI_SMALL                       108
#define IDC_DYNASVR                     109
#define IDR_MAINFRAME                   128
#define IDB_BITMAP_EARTH                129
#define IDD_DIALOG_SETTING              130
#define IDB_BITMAP_LOGO                 131
#define IDC_CONTEXTMAIN                 135
#define IDI_DISABLED                    137
#define IDC_INSTALL                     1001
#define IDC_REMOVE                      1002
#define IDC_SERVICENAME                 1004
#define IDC_SERVICEEXE                  1005
#define IDC_BROWSE                      1006
#define IDC_INTERACTIVE                 1007
#define IDC_MYGROUP_ONLY                1010
#define IDC_LISTWIN32OWN                1011
#define IDC_SERVICESLIST                1012
#define IDC_ALL                         1015
#define IDC_BUTTON_ABOUT                1017
#define IDC_STARTUP_BOX                 1020
#define IDC_STARTUP                     1021
#define IDC_README                      1022
#define IDC_SVRCTRL                     1023
#define IDC_SVRSTATUS                   1024
#define IDI_C_ABOUT                     32772
#define IDI_C_START                     32773
#define IDI_C_STOP                      32774
#define IDI_C_INFO                      32776
#define ID_MENUITEM32777                32777
#define IDI_C_RESTORE                   32778
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
